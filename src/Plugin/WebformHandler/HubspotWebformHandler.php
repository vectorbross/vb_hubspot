<?php

namespace Drupal\vb_hubspot\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use SevenShores\Hubspot\Exceptions\HubspotException;
use SevenShores\Hubspot\Factory as HubspotFactory;

/**
 * Submits .
 *
 * @WebformHandler(
 *   id = "hubspot_webform_handler",
 *   label = @Translation("Hubspot Webform Handler"),
 *   category = @Translation("Transaction"),
 *   description = @Translation("Sends the submission data to Hubspot."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class HubspotWebformHandler extends WebformHandlerBase {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'list_id' => '',
      'email' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $hs_config = \Drupal::config('vb_hubspot.settings');
    $form['list_id'] = [
      '#type' => 'select',
      '#title' => $this->t('List'),
      '#options' => ['' => $this->t('Select a list')],
      '#weight' => 0,
    ];
    $api_key = $hs_config->get('api_key');
    $hubspot = HubspotFactory::create($api_key);
    $result = $hubspot->contactLists()->all();
    foreach($result->data->lists as $list) {
      if(!$list->dynamic) {
        $form['list_id']['#options'][$list->listId] = $list->name;
      }
    }
    if(isset($form['list_id']['#options'][$this->configuration['list_id']])) {
      $form['list_id']['#default_value'] = $this->configuration['list_id'];
    }
    $elements = $this->webform->getElementsDecodedAndFlattened();
    foreach($elements as $key => $element) {
      if(!in_array($element['#type'], ['textfield', 'email'])) {
        unset($elements[$key]);
      }
    }
    $options = [];
    foreach($elements as $key => $element) {
      $options[$key] = $element['#title'];
    }
    $form['email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->configuration['email'],
    ];
    if(!empty($this->configuration['list_id'])) {
      // TODO: custom fields
      /*$wrap = new \CS_REST_Lists($this->configuration['list_id'], $cm_config->get('api_key'));
      $result = $wrap->get_custom_fields();
      if($result->was_successful() && is_array($result->response)) {
        foreach($result->response as $response) {
          $key = str_replace(['[', ']'], '', strtolower($response->Key));
          $form[$key] = [
            '#type' => 'select',
            '#title' => $response->FieldName,
            '#options' => ['' => ''] + $options,
            '#default_value' => $this->configuration[$key],
          ];
        }
      }*/
    }
    return $form;
  }
//https://youtu.be/J88hUhlIIVc
  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['list_id'] = $form_state->getValue('list_id');
    $this->configuration['email'] = $form_state->getValue('email');
    // TODO: custom fields
    /*if(!empty($this->configuration['list_id'])) {
      $hs_config = \Drupal::config('vb_hubspot.settings');
      $wrap = new \CS_REST_Lists($this->configuration['list_id'], $hs_config->get('api_key'));
      $result = $wrap->get_custom_fields();
      if($result->was_successful() && is_array($result->response)) {
        foreach($result->response as $response) {
          $key = str_replace(['[', ']'], '', strtolower($response->Key));
          $this->configuration[$key] = $form_state->getValue($key);
        }
      }
    }*/
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state,  WebformSubmissionInterface $webform_submission) {
    $hs_config = \Drupal::config('vb_hubspot.settings');
    $values = $webform_submission->getData();
    $keys = [];
    $hubspot = HubspotFactory::create($hs_config->get('api_key'));
    $data = [
      [
        'property' => 'email',
        'value' => $values[$this->configuration['email']],
      ],
    ];
    try {
      $result = $hubspot->contacts()->create($data);
    } catch(HubspotException $e) {
      \Drupal::logger('vb_husbpot')->error($e->getMessage());
    }
    if(isset($result['vid']) && is_numeric($result['vid']) && !empty($this->configuration['list_id'])) {
      $result = $hubspot->contactLists()->addContact($this->configuration['list_id'], [$result->vid]);
    }
  }
}
